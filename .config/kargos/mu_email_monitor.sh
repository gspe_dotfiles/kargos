#!/bin/sh
# Script to monitor mu email

new="$(mu find flag:unread AND NOT flag:trashed | wc -l)"
commercialista="$(mu find \(contact:marchini OR contact:di nonno\) AND flag:new | wc -l)"

echo "$new | iconName=mail-unread"

if [ $commercialista -gt 0 ]
   then
       echo "$commercialista | iconName=mail-mark-important"
fi

echo "---"
echo "$commercialista | iconName=mail-mark-important"
